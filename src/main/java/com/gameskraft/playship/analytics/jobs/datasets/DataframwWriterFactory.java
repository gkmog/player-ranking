package com.gameskraft.playship.analytics.jobs.datasets;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public class DataframwWriterFactory {
	public static final String SCORE_WRITER = "scorewriter";
	@Getter
	private static final DataframwWriterFactory instance = new DataframwWriterFactory();
	private static final Map<String, IDataframeWriter> writers = new HashMap<String, IDataframeWriter>();
	static {
		writers.put(SCORE_WRITER, new ScoreWriter());
	}

	public final IDataframeWriter getWriter(String name) {
		return writers.get(name);
	}

}
