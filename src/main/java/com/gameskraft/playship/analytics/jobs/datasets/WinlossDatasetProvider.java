package com.gameskraft.playship.analytics.jobs.datasets;

import com.gameskraft.playship.analytics.config.Config;
import com.gameskraft.playship.analytics.config.ConfigFactory;
import com.gameskraft.playship.analytics.jobs.RatingUpdateTime;
import lombok.extern.log4j.Log4j;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Function1;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

@Log4j
public class WinlossDatasetProvider implements IDatasetProvider {

	public static final String OPPONENT_COL_NAME = "opponent";
	public static final String PLAYER_COL_NAME = "player";
	public static final String RES_COL_NAME = "result";
	public static final String MATCH_ID_COL_NAME = "match_id";
	public static final String PK_COL_NAME = "id";

	private static final String jdbcQueryTemplate = "select cast(own_match.id as char) as id , "
			+ "cast(own_match.match_id as char) as match_id,  " + "own_match.user_id as player,  "
			+ "opp_match.user_id as opponent, " + "case " + "  when own_match.is_winner = 1 then 1 "
			+ "  when own_match.is_winner = 2 then 0 " + "  when own_match.is_winner = 3 then .5 "
			+ "  when own_match.is_winner = 4 then 0 " + "  when own_match.is_winner = 5 then .5 " + "  else .5 "
			+ "  end " + " as result " + "from jarvis.match_players own_match, " + "jarvis.match_players opp_match "
			+ "where own_match.match_id=opp_match.match_id " + "and own_match.user_id <> opp_match.user_id "
			+ "and own_match.is_winner not in (6,5) " + "and own_match.match_id > 3 " + "and own_match.game_id = %s "
			+ " " + "" + "and own_match.start_time > \"%s\"";

	// private static final String testDataFileName =
	// "/Users/saikatmanna/testdata/ranking/gameresults.csv";
	private static final String testDataFileName = "/Users/saikatmanna/testdata/ranking/snake_results_prod.csv";

	@Override
	public Dataset<Row> provide(SparkSession ses, int gameType) {
		// return loadCSV(ses, gameType);
		// return loadFromCassandra(ses);
		return loadFromJDBC(ses, gameType);
	}

	private Dataset<Row> loadFromJDBC(SparkSession ses, int gameType) {

		Config config = ConfigFactory.getConfig();
		Map<String, String> options = new HashMap<String, String>();
		options.put("driver", "com.mysql.jdbc.Driver");
		options.put("user", config.getJdbcUsername());
		options.put("password", config.getJdbcPwd());
		options.put("url", config.getJdbcURL());
		options.put("dbtable", createJDBCQuery(ses, gameType));
		SQLContext sqlContext = ses.sqlContext();
		Dataset<Row> df = sqlContext.read().format("jdbc").options(options).load();
		df.printSchema();
		// do a type conversion
		@SuppressWarnings("unchecked")
		Dataset<Row> dfConverted = df.map((Function1<Row, Row> & Serializable) ((row) -> {
			String id = row.getAs("id");
			String match_id = row.getAs("match_id");
			long player = row.getAs("player");
			long opponent = row.getAs("opponent");
			BigDecimal resD = row.getAs("result");

			return RowFactory.create(String.valueOf(id), String.valueOf(match_id), player, opponent,
					resD.doubleValue());
		}), RowEncoder.apply(getSchema()));
		// df.explain();
		return dfConverted;

	}

	private String createJDBCQuery(SparkSession ses, int gameType) {
		Long lastRatingUpdateTime = RatingUpdateTime.getInstance().getLastRatingUpdateTime(ses, gameType);
		java.text.SimpleDateFormat sdf =
				new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		String lastUpdateTime = sdf.format(new Date(lastRatingUpdateTime));
		log.info("date : " + lastUpdateTime);
		String query = String.format(jdbcQueryTemplate, gameType, lastUpdateTime);
		query = "(" + query + ")" + "as user_match";
		log.info("Win loss data query: " + query);
		return query;
	}

	@SuppressWarnings("unchecked")
	private Dataset<Row> loadFromCassandra(SparkSession ses) {
		SQLContext sqlContext = ses.sqlContext();
		Map<String, String> cassOptions = new HashMap<String, String>();
		cassOptions.put("keyspace", "game_event");
		cassOptions.put("table", "game_user");

		Dataset<Row> df = sqlContext.read().format("org.apache.spark.sql.cassandra").options(cassOptions).load();
		df = df.filter(df.col("event_type").equalTo(104));
		df = df.select("user_id", "game_id", "data_int_2");
		df = df.map((Function1<Row, Row> & Serializable) ((row) -> {
			String gameId = row.getAs("game_id");
			int dInt2 = row.getAs("data_int_2");
			int player = row.getAs("user_id");
			int opponent = -1;// for now
			// generate a PK
			String pk = gameId + "_" + player;
			//
			double result = -1;

			switch (dInt2) {
			case 1:
				result = 1;
				break;
			case 2:
			case 4:
				result = 0;
				break;
			case 3:
			case 5:
				result = .5;
			default:
				result = .5;
			}

			return RowFactory.create(pk, gameId, (long) player, (long) opponent, result);
		}), RowEncoder.apply(getSchema()));
		// df.explain();
		return df;
	}

	private Dataset<Row> loadCSV(SparkSession ses, int gameType) {

		Dataset<Row> dataset = ses.read().option("header", "true").schema(getSchema()).csv(testDataFileName);
		return dataset;
	}

	private StructType getSchema() {

		StructType schema = new StructType(
				new StructField[] { new StructField(PK_COL_NAME, DataTypes.StringType, false, Metadata.empty()),
						new StructField(MATCH_ID_COL_NAME, DataTypes.StringType, false, Metadata.empty()),
						new StructField(PLAYER_COL_NAME, DataTypes.LongType, false, Metadata.empty()),
						new StructField(OPPONENT_COL_NAME, DataTypes.LongType, false, Metadata.empty()),
						new StructField(RES_COL_NAME, DataTypes.DoubleType, false, Metadata.empty()), });
		return schema;
	}

}
