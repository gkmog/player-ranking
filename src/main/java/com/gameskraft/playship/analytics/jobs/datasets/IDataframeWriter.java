package com.gameskraft.playship.analytics.jobs.datasets;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public interface IDataframeWriter {

	public void write(Dataset<Row> df, int gameType);
}
