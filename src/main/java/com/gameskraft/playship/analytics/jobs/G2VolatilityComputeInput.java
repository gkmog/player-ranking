package com.gameskraft.playship.analytics.jobs;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class G2VolatilityComputeInput implements Serializable {

	private double currentVolatility;

	private double convergence;
	private double big_delta;
	private double currentDeviation;
	private double v;
	private double tau;
	
	private long player;
}
