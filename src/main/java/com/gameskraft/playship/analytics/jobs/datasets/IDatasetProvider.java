package com.gameskraft.playship.analytics.jobs.datasets;

import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public interface IDatasetProvider {

	public Dataset<Row> provide(SparkSession ses,int gameType);
}
