package com.gameskraft.playship.analytics.jobs;

import java.io.Serializable;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class G2VnDeltaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2015798969293761060L;
	@NonNull
	private double v ;
	@NonNull
	private double delta;
}
