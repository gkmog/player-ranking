package com.gameskraft.playship.analytics.jobs;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.util.function.Function;

@Log4j
public class Glicko2Calculations {

	@RequiredArgsConstructor
	private static final class ItrRes {
		@NonNull
		double upperBound, lowerBound;
	}

	public static double calculateNewVolatiliy(G2VolatilityComputeInput in) {
		log.debug("SAIKAT: calculating volatility for " + in.getPlayer());
		double upperBound = getUpperBoundOfItr(in);
		Function<Double, Double> fx = createNormalizationFunc(in);
		double lowerBound = getLowerBound(in, upperBound, fx);
		log.debug("SAIKAT: lowerbound = " + lowerBound + ", upperbound =" + upperBound);

		double nv = iterate(upperBound, lowerBound, in, fx);
		log.debug("SAIKAT: nv = " + nv);
		return nv;
	}

	private static double iterate(final double upperBound, final double lowerBound, G2VolatilityComputeInput in,
			Function<Double, Double> fx) {

		double FA = fx.apply(upperBound);
		double FB = fx.apply(lowerBound);
		double A = upperBound;
		double B = lowerBound;

		while (Math.abs(A - B) > in.getConvergence()) {
			// C= A + (A − B)fA/(fB − fA),
			double C = A + (A - B) * FA / (FB - FA);
			double FC = fx.apply(C);
			if (FC * FB < 0) {
				A = B;
				FA = FB;
			} else {
				FA = FA / 2;
			}
			B = C;
			FB = FC;
		}
		double nv = Math.exp(A / 2);
		return nv;
	}

	private static double getLowerBound(G2VolatilityComputeInput in, final double upperBound,
			Function<Double, Double> fx) {
		double currDevSqr = in.getCurrentDeviation() * in.getCurrentDeviation();
		double bigDeltaSqr = in.getBig_delta() * in.getBig_delta();
		if (bigDeltaSqr > (currDevSqr + in.getV()))
			return Math.log(bigDeltaSqr - currDevSqr - in.getV());
		double k = 1;
		log.debug("lowerbound itr");
		while (fx.apply(upperBound - k * in.getTau()) < 0) {
			k++;
		}
		log.debug("k=" + k);
		return upperBound - k * in.getTau();
	}

	private static double getUpperBoundOfItr(G2VolatilityComputeInput in) {

		return Math.log(in.getCurrentVolatility() * in.getCurrentVolatility());
	}

	private static Function<Double, Double> createNormalizationFunc(G2VolatilityComputeInput in) {
		final double bigDeltaSqr = in.getBig_delta() * in.getBig_delta();
		final double currentDevSqr = in.getCurrentDeviation() * in.getCurrentDeviation();
		final double v = in.getV();
		final double a = getUpperBoundOfItr(in);
		final double tauSqr = in.getTau() * in.getTau();

		return (x) -> {
			double eToX = Math.exp(x);
			double t1 = eToX * (bigDeltaSqr - currentDevSqr - v - eToX) / (2 * Math.pow((currentDevSqr + v + eToX), 2));
			double t2 = (x - a) / tauSqr;
			return t1 - t2;
		};
	}

	public static void main(String... args) {

		G2VolatilityComputeInput in = G2VolatilityComputeInput.builder().v(0.5459158278564125)
				.big_delta(-1.7697089923332836).currentDeviation(2.014761872416068).currentVolatility(0.06).tau(.5)
				.convergence(0.00001).build();
		double nv = calculateNewVolatiliy(in);
		log.debug(nv);
	}

}
