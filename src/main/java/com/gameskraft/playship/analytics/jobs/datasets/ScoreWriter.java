package com.gameskraft.playship.analytics.jobs.datasets;

import com.mongodb.spark.MongoSpark;
import lombok.extern.log4j.Log4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

@Log4j
public class ScoreWriter implements IDataframeWriter {

	private static final String base_dir = "/Users/saikatmanna/testdata/ranking/results/";

	@Override
	public void write(Dataset<Row> df, int gameType) {

		// writeAsCSV(df, gameType);
		writeIntoMongoDB(df);
	}

	private void writeAsCSV(Dataset<Row> df, int gameType) {

		String outfilename = base_dir + "_ranking_" + gameType + ".csv";
		df.repartition(1).write().option("header", "true").csv(outfilename);
	}

	private void writeIntoMongoDB(Dataset<Row> df) {
		log.info("writing new user rating into mongoDB");
		MongoSpark.write(df).format("com.mongodb.spark.sql.DefaultSource").mode("append").save();
	}

}
