package com.gameskraft.playship.analytics.jobs;

import com.gameskraft.playship.analytics.jobs.datasets.DataframwWriterFactory;
import com.gameskraft.playship.analytics.jobs.datasets.DatasetProviderFactory;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class PlayerRankingJob implements ISparkJob {

	private final int[] gameTypes = { 204, 205, 214 };
	@Override
	public void run(SparkSession ses) {

		// get the initial dataset
		for (int gameType : gameTypes) {
			computeNewRating(ses, gameType);
		}
		//RatingKafkaProducer.runProducer();
	}

	private void computeNewRating(SparkSession ses, int gameType) {
		DatasetProviderFactory dsFactory = DatasetProviderFactory.getInstance();
		Dataset<Row> winlossDataset = dsFactory.getProvider(DatasetProviderFactory.WIN_LOSS_DS).provide(ses, gameType);
		// winlossDataset.show(20);
		// get the existing player ratings
		Dataset<Row> existingRating = dsFactory.getProvider(DatasetProviderFactory.EXISTING_RATING).provide(ses,
				gameType);
		// existingRating.show(20);

		Dataset<Row> allUsers = dsFactory.getProvider(DatasetProviderFactory.ALL_USERS).provide(ses, gameType);
		// allUsers.show(10);

		Glicko2 ratingCalculator = new Glicko2();
		Dataset<Row> newRating = ratingCalculator.computeNewRating(ses, winlossDataset, existingRating, allUsers, gameType);
		// write
		DataframwWriterFactory.getInstance().getWriter(DataframwWriterFactory.SCORE_WRITER).write(newRating, gameType);
		RatingUpdateTime.getInstance().updateRatingUpdateTime(ses, gameType);
	}

}
