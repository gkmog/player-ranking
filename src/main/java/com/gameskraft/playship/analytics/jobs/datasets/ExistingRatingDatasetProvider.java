package com.gameskraft.playship.analytics.jobs.datasets;

import com.mongodb.spark.MongoSpark;
import lombok.extern.log4j.Log4j;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

@Log4j
public class ExistingRatingDatasetProvider implements IDatasetProvider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1683888216814083315L;
	public static final String RATING_COL_NAME = "rating";
	public static final String RATING_DEV_COL_NAME = "deviation";
	public static final String PLAYER_COL_NAME = "player";
	public static final String VOLATILITY_COL_NAME = "volatility";

	// private static final String testDataFileName =
	// "/Users/saikatmanna/testdata/ranking/currentscores.csv";
	private static final String testDataFileName = "/Users/saikatmanna/testdata/ranking/prod_users.csv";

	@Override
	public Dataset<Row> provide(SparkSession ses, int gameType) {
		//return loadCSV(ses, gameType);
		return loadFromMongoDB(ses, gameType);
	}

	private Dataset<Row> loadCSV(SparkSession ses, int gameType) {

		StructType schema = new StructType(
				new StructField[] { new StructField(PLAYER_COL_NAME, DataTypes.LongType, false, Metadata.empty()),
						new StructField(RATING_COL_NAME, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(RATING_DEV_COL_NAME, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(VOLATILITY_COL_NAME, DataTypes.DoubleType, false, Metadata.empty()) });

		Dataset<Row> dataset = ses.read().option("header", "true").schema(schema).csv(testDataFileName);
		return dataset;
	}

	private Dataset<Row> loadFromMongoDB(SparkSession ses, int gameType) {

		JavaSparkContext jsc = new JavaSparkContext(ses.sparkContext());
		log.info("loading user rating from mongoDB");
		Dataset<Row> implicitDS = MongoSpark.load(jsc).toDF();
		implicitDS = implicitDS.filter("gameType = " + gameType);
		implicitDS.printSchema();
		return implicitDS;
	}

}
