package com.gameskraft.playship.analytics.jobs;

import org.apache.kafka.clients.producer.*;

import java.util.Properties;

public class RatingKafkaProducer {

    private final static String TOPIC = "bucketing-service-redisClearEvent";
    private final static String BOOTSTRAP_SERVERS = "b-2.kafkastage.j9co45.c2.kafka.ap-south-1.amazonaws.com:9092,b-1.kafkastage.j9co45.c2.kafka.ap-south-1.amazonaws.com:9092";

    static void runProducer() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(properties);
        try{
            kafkaProducer.send(new ProducerRecord<String, String>(TOPIC, null, "clear_redis" ));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaProducer.close();
        }
    }
}
