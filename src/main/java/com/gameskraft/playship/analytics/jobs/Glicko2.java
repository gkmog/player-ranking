package com.gameskraft.playship.analytics.jobs;

import com.gameskraft.playship.analytics.jobs.datasets.AllUsers;
import com.gameskraft.playship.analytics.jobs.datasets.ExistingRatingDatasetProvider;
import com.gameskraft.playship.analytics.jobs.datasets.WinlossDatasetProvider;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Function1;
import scala.Tuple2;

import java.io.Serializable;

import static org.apache.spark.sql.functions.*;

public class Glicko2 implements Serializable {

	private static final String g2_player_col_name = "player";

	private static final String g2_rating_col_name = "g2_rating";

	private static final String g2_deviation_col_name = "g2_deviation";

	private static final String opponent_prefix = "opponent";

	private static final String big_delta_col_name = "big_delta";

	private static final String v_col_name = "v_sum";

	private static final String new_voltility_col_name = "new_volatility";

	private static final String temp_playerid_col_name = "temp_playerId";

	private static final String delta_sum_col_name = "delta_sum";

	private static final String own_prefix = "own";

	private static final String new_rating_col_name = "new_rating";

	private static final String new_deviation_col_name = "new_deviation";

	private static final double tau = 0.5;

	private static final double convergence = 0.00001;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Dataset<Row> computeNewRating(SparkSession ses, Dataset<Row> games, Dataset<Row> currentRating, Dataset<Row> allUsers, int gameType) {

		currentRating.cache();
		Dataset<Row> playerWithNoRecord = currentRating.join(games, games.col(WinlossDatasetProvider.OPPONENT_COL_NAME)
				.equalTo(currentRating.col(g2_player_col_name)), "leftanti").drop("updated_at");
		currentRating = populateAllUsersWithRatings(currentRating, allUsers);
		// step 2. convert to glicko2 scale
		Dataset<Row> currRatingG2 = convertToG2(currentRating);
		// join with player so that player rating is populated
		Dataset<Row> dsWithPlayerScore = enrichWithOpponentrating(games, currRatingG2);
		dsWithPlayerScore = enrichWithOwnrating(dsWithPlayerScore, currRatingG2);
		dsWithPlayerScore = enrichWithV(dsWithPlayerScore);
		Dataset<Row> summedUpDeltasNVs = sumUpDeltasAndVs(dsWithPlayerScore, ses);
		currRatingG2 = enrichWithSummedUpDeltasAndVs(currRatingG2, summedUpDeltasNVs);
		currRatingG2 = currRatingG2.withColumn("big_delta",
				currRatingG2.col(v_col_name).multiply(currRatingG2.col(delta_sum_col_name)));
		currRatingG2 = enrichWithNewValues(currRatingG2);
		// currRatingG2.show(1000);
		// get a javaRDD , as Dataset does not have reducebykey!!

		currRatingG2 = enrichWithGameType(currRatingG2, gameType);
		currRatingG2 = calculateNewRatingDeviation(playerWithNoRecord, currRatingG2);
		//adding last updated field into the dataset
		currRatingG2 = currRatingG2.withColumn("updated_at", current_timestamp().as("updated_at"));

		return currRatingG2;

	}

	private Dataset<Row> calculateNewRatingDeviation(Dataset<Row> playerWithNoRecord, Dataset<Row> currRatingG2) {

		//calculating new rating deviation for players which does not compete during the rating period
		playerWithNoRecord = playerWithNoRecord.withColumn("new_deviation",
				functions.sqrt(playerWithNoRecord.col("new_volatility").multiply(playerWithNoRecord.col("new_volatility"))
						.plus((playerWithNoRecord.col("new_deviation").multiply(playerWithNoRecord.col("new_deviation"))).divide(173.7178 * 173.7178))
				).multiply(173.7178));
		currRatingG2 = currRatingG2.unionByName(playerWithNoRecord);
		return currRatingG2;
	}

	private Dataset<Row> enrichWithGameType(Dataset<Row> currRating, int gameType) {

		currRating = currRating.withColumn("gameType", functions.lit(gameType));
		// have to add _id with player_gameType
		currRating = currRating.withColumn("_id", concat(currRating.col("player"), lit("_"), currRating.col("gameType")));
		return currRating;
	}

	private Dataset<Row> populateAllUsersWithRatings(Dataset<Row> currentRating, Dataset<Row> allUsers) {

		currentRating = currentRating.drop("_id", "g2_rating", "g2_deviation", "volatility", "v_sum", "delta_sum", "big_delta", "gameType", "updated_at")
				.withColumnRenamed("new_volatility", "volatility")
				.withColumnRenamed("new_rating", "rating")
				.withColumnRenamed("new_deviation", "deviation");
		Dataset<Row> joinedDF = allUsers.join(currentRating, allUsers.col(AllUsers.USER_ID).equalTo(currentRating.col(g2_player_col_name)), "left");
		joinedDF = joinedDF.na().fill(1500.00, new String[]{ExistingRatingDatasetProvider.RATING_COL_NAME});
		joinedDF = joinedDF.na().fill(350.00, new String[]{ExistingRatingDatasetProvider.RATING_DEV_COL_NAME});
		joinedDF = joinedDF.na().fill(0.06, new String[]{ExistingRatingDatasetProvider.VOLATILITY_COL_NAME});
		joinedDF = joinedDF.drop(g2_player_col_name);
		joinedDF = joinedDF.withColumnRenamed(AllUsers.USER_ID, g2_player_col_name);
		joinedDF.printSchema();

		return joinedDF;
	}

	private Dataset<Row> enrichWithNewValues(Dataset<Row> users) {

		StructType schema = new StructType(
				new StructField[] { new StructField("temp_playerId", DataTypes.LongType, false, Metadata.empty()),
						new StructField(new_voltility_col_name, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(new_rating_col_name, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(new_deviation_col_name, DataTypes.DoubleType, false, Metadata.empty()) });

		@SuppressWarnings("unchecked")
		Dataset<Row> dsWithNewValues = users.map((Function1<Row, Row> & Serializable) ((row) -> {
			long playerId = row.getAs(ExistingRatingDatasetProvider.PLAYER_COL_NAME);

			double big_delta = row.getAs(big_delta_col_name);
			double currentDeviation = row.getAs(g2_deviation_col_name);
			double currentVolatility = row.getAs(ExistingRatingDatasetProvider.VOLATILITY_COL_NAME);
			double current_g2_rating = row.getAs(g2_rating_col_name);
			double v = row.getAs(v_col_name);
			double delta_sum = row.getAs(delta_sum_col_name);
			long player = row.getAs(ExistingRatingDatasetProvider.PLAYER_COL_NAME);
			// compute the new volatility
			G2VolatilityComputeInput in = G2VolatilityComputeInput.builder().big_delta(big_delta).tau(tau)
					.player(player).currentDeviation(currentDeviation).currentVolatility(currentVolatility)
					.convergence(convergence).v(v).build();
			double newVolatility = Glicko2Calculations.calculateNewVolatiliy(in);
			double effective_g2_rd = Math.sqrt(currentDeviation * currentDeviation + newVolatility * newVolatility);
			double updated_g2_rd = 1 / Math.sqrt(1 / (effective_g2_rd * effective_g2_rd) + 1 / v);
			double updated_g2_rating = current_g2_rating + updated_g2_rd * updated_g2_rd * delta_sum;
			double new_deviation = 173.7178 * updated_g2_rd;
			double new_rating = 173.7178 * updated_g2_rating + 1500;
			return RowFactory.create(playerId, newVolatility, new_rating, new_deviation);
		}), RowEncoder.apply(schema));

		Dataset<Row> joinedDF = users.join(dsWithNewValues, users.col(ExistingRatingDatasetProvider.PLAYER_COL_NAME)
				.equalTo(dsWithNewValues.col(temp_playerid_col_name)));
		joinedDF = joinedDF.drop(temp_playerid_col_name);
		return joinedDF;
	}

	private Dataset<Row> sumUpDeltasAndVs(Dataset<Row> vEnrichedDataset, SparkSession ses) {

		// extract only the values
		JavaPairRDD<Long, G2VnDeltaBean> vValues = vEnrichedDataset.toJavaRDD()
				.mapToPair((PairFunction<Row, Long, G2VnDeltaBean>) (row -> {
					double v = row.getAs("g2_v");
					double delta = row.getAs("g2_delta");
					long playerId = row.getAs(g2_player_col_name);
					return Tuple2.apply(playerId, new G2VnDeltaBean(v, delta));
				}));
		// now reduce it playerId
		JavaPairRDD<Long, G2VnDeltaBean> summedUpVs = vValues
				.reduceByKey((Function2<G2VnDeltaBean, G2VnDeltaBean, G2VnDeltaBean> & Serializable) (acc, bean) -> {
					double delta = acc.getDelta() + bean.getDelta();
					double v = acc.getV() + bean.getV();
					return new G2VnDeltaBean(v, delta);
				});
		JavaRDD<Row> jRdd = summedUpVs.map(tuple -> {
			return RowFactory.create(tuple._1, 1 / tuple._2.getV(), tuple._2.getDelta());
		});

		StructType schema = new StructType(
				new StructField[] { new StructField("temp_playerId", DataTypes.LongType, false, Metadata.empty()),
						new StructField(v_col_name, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField("delta_sum", DataTypes.DoubleType, false, Metadata.empty()) });
		return ses.createDataFrame(jRdd, schema);

	}

	private Dataset<Row> enrichWithV(Dataset<Row> gameResultsWithRatings) {

		StructType schema = new StructType(
				new StructField[] { new StructField("v_pk", DataTypes.StringType, false, Metadata.empty()),
						new StructField("g2_v", DataTypes.DoubleType, false, Metadata.empty()),
						new StructField("g2_gPhi", DataTypes.DoubleType, false, Metadata.empty()),
						new StructField("g2_Erating", DataTypes.DoubleType, false, Metadata.empty()),
						new StructField("g2_delta", DataTypes.DoubleType, false, Metadata.empty()) });

		@SuppressWarnings("unchecked")
		Dataset<Row> vDs = gameResultsWithRatings.map((Function1<Row, Row> & Serializable) ((row) -> {

			String id = row.getAs(WinlossDatasetProvider.PK_COL_NAME);
			double opponentRating = row.getAs(SqlUtils.appendAlias(opponent_prefix, g2_rating_col_name));
			double opponentDeviation = row.getAs(SqlUtils.appendAlias(opponent_prefix, g2_deviation_col_name));
			double ownRating = row.getAs(SqlUtils.appendAlias(own_prefix, g2_rating_col_name));
			double gPhi = 1 / Math.sqrt(1 + 3 * (opponentDeviation * opponentDeviation) / (Math.PI * Math.PI));
			double Eratings = 1 / (1 + Math.pow(Math.E, -1 * gPhi * (ownRating - opponentRating)));
			double g2_v = gPhi * gPhi * Eratings * (1 - Eratings);
			double res = row.getAs(WinlossDatasetProvider.RES_COL_NAME);
			double g2_delta = gPhi * (res - Eratings);
			return RowFactory.create(id, g2_v, gPhi, Eratings, g2_delta);

		}), RowEncoder.apply(schema));

		// make a join

		Dataset<Row> joinedDF = gameResultsWithRatings.join(vDs,
				gameResultsWithRatings.col(WinlossDatasetProvider.PK_COL_NAME).equalTo(vDs.col("v_pk")));
		joinedDF = joinedDF.drop("v_pk");
		return joinedDF;
	}

	@SuppressWarnings("unchecked")
	private Dataset<Row> convertToG2(Dataset<Row> currentRating) {

		StructType schema = new StructType(
				new StructField[] { new StructField(g2_player_col_name, DataTypes.LongType, false, Metadata.empty()),
						new StructField(g2_rating_col_name, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(g2_deviation_col_name, DataTypes.DoubleType, false, Metadata.empty()),
						new StructField(ExistingRatingDatasetProvider.VOLATILITY_COL_NAME, DataTypes.DoubleType, false,
								Metadata.empty()) });

		Dataset<Row> g2ratings = currentRating.map((Function1<Row, Row> & Serializable) ((row) -> {
			double rating = ((Number) row.getAs(ExistingRatingDatasetProvider.RATING_COL_NAME)).doubleValue();
			double deviation = ((Number) row.getAs(ExistingRatingDatasetProvider.RATING_DEV_COL_NAME)).doubleValue();
			double g2rating = (rating - Glick2Constants.g2baseRank) / Glick2Constants.g2factor;
			double g2deviation = ((Number) deviation).doubleValue() / Glick2Constants.g2factor;
			long player = row.getAs(ExistingRatingDatasetProvider.PLAYER_COL_NAME);
			double volatility = row.getAs(ExistingRatingDatasetProvider.VOLATILITY_COL_NAME);
			return RowFactory.create(player, g2rating, g2deviation, volatility);
		}), RowEncoder.apply(schema));
		return g2ratings;
	}

	private Dataset<Row> enrichWithOpponentrating(Dataset<Row> games, Dataset<Row> g2rating) {

		// alias the ratings
		Dataset<Row> aliasedRating = g2rating.select(g2rating.col(g2_player_col_name).as("opponent_id"),
				g2rating.col(g2_rating_col_name).as(SqlUtils.appendAlias(opponent_prefix, g2_rating_col_name)),
				g2rating.col(g2_deviation_col_name).as(SqlUtils.appendAlias(opponent_prefix, g2_deviation_col_name)));
		Dataset<Row> joinedDF = games.join(aliasedRating,
				games.col(WinlossDatasetProvider.OPPONENT_COL_NAME).equalTo(aliasedRating.col("opponent_id")));
		joinedDF = joinedDF.drop("opponent_id");
		return joinedDF;

	}

	private Dataset<Row> enrichWithSummedUpDeltasAndVs(Dataset<Row> users, Dataset<Row> vSums) {

		// alias the ratings

		Dataset<Row> joinedDF = users.join(vSums,
				users.col(ExistingRatingDatasetProvider.PLAYER_COL_NAME).equalTo(vSums.col("temp_playerId")));
		joinedDF = joinedDF.drop("temp_playerId");
		return joinedDF;

	}

	private Dataset<Row> enrichWithOwnrating(Dataset<Row> games, Dataset<Row> g2rating) {

		// alias the ratings
		Dataset<Row> aliasedRating = g2rating.select(g2rating.col(g2_player_col_name).as("other_id"),
				g2rating.col(g2_rating_col_name).as(SqlUtils.appendAlias(own_prefix, g2_rating_col_name)),
				g2rating.col(g2_deviation_col_name).as(SqlUtils.appendAlias(own_prefix, g2_deviation_col_name)));
		Dataset<Row> joinedDF = games.join(aliasedRating,
				games.col(WinlossDatasetProvider.PLAYER_COL_NAME).equalTo(aliasedRating.col("other_id")));
		joinedDF = joinedDF.drop("other_id");
		return joinedDF;

	}

}
