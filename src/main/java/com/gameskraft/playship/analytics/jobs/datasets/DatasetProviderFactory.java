package com.gameskraft.playship.analytics.jobs.datasets;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class DatasetProviderFactory {

	public static final String WIN_LOSS_DS = "winlossdsprovider";
	public static final String EXISTING_RATING = "existingratingprovider";
	public static final String ALL_USERS = "allusers";

	@Getter
	private static final DatasetProviderFactory instance = new DatasetProviderFactory();

	private static final Map<String, IDatasetProvider> providers = new HashMap<String, IDatasetProvider>();

	static {

		providers.put(WIN_LOSS_DS, new WinlossDatasetProvider());
		providers.put(EXISTING_RATING, new ExistingRatingDatasetProvider());
		providers.put(ALL_USERS, new AllUsers());

	}

	public IDatasetProvider getProvider(String name) {
		return providers.get(name);

	}

}
