package com.gameskraft.playship.analytics.jobs;

import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.ReadConfig;
import com.mongodb.spark.config.WriteConfig;
import lombok.Getter;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.*;

import java.util.*;

public class RatingUpdateTime {

    @Getter
    private static final RatingUpdateTime instance = new RatingUpdateTime();

    public Long getLastRatingUpdateTime(SparkSession ses, int gameType) {

        JavaSparkContext jsc = new JavaSparkContext(ses.sparkContext());
        Map<String, String> readOverrides = new HashMap<String, String>();
		readOverrides.put("collection", "rating_update_time");
		readOverrides.put("readPreference.name", "secondaryPreferred");
		ReadConfig readConfig = ReadConfig.create(jsc).withOptions(readOverrides);
        Dataset<Row> implicitDS = MongoSpark.load(jsc, readConfig).toDF();
        implicitDS = implicitDS.filter("_id = " + gameType);
        implicitDS.printSchema();
        List<Long> list = implicitDS.select(implicitDS.col("date")).as(Encoders.LONG()).collectAsList();
        return list.get(0);
    }

    public void updateRatingUpdateTime(SparkSession ses, int gameType) {

        StructType schema = new StructType(new StructField[] {
                new StructField("date", DataTypes.LongType, false, Metadata.empty()),
                new StructField("_id", DataTypes.IntegerType, false, Metadata.empty()),
        });

        JavaSparkContext jsc = new JavaSparkContext(ses.sparkContext());
        Map<String, String> writeOverrides = new HashMap<String, String>();
        writeOverrides.put("collection", "rating_update_time");
        writeOverrides.put("writeConcern.w", "majority");
        WriteConfig writeConfig = WriteConfig.create(jsc).withOptions(writeOverrides);
        long date = new Date().getTime();
        List<Row> data = new ArrayList<>();
        data.add(RowFactory.create(date, gameType));
        Dataset<Row> ds = ses.createDataFrame(data, schema).toDF();
        MongoSpark.save(ds.write().mode("append"), writeConfig);
    }
}
