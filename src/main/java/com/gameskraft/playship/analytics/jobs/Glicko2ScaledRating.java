package com.gameskraft.playship.analytics.jobs;

import java.io.Serializable;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class Glicko2ScaledRating implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9084126689534713034L;
	@NonNull
	private int player;
	@NonNull
	private double rating;
	@NonNull
	private double ratingDeviation;

}
