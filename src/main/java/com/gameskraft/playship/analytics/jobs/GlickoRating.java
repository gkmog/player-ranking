package com.gameskraft.playship.analytics.jobs;

import java.io.Serializable;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class GlickoRating implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 885839441080762311L;
	@NonNull
	int player;
	@NonNull
	double raking;
	@NonNull
	double rankingDeviation;
	@NonNull
	double volatility;

}
