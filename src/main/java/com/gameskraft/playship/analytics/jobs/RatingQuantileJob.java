package com.gameskraft.playship.analytics.jobs;

import com.gameskraft.playship.analytics.jobs.datasets.DatasetProviderFactory;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Log4j
public class RatingQuantileJob implements ISparkJob {

	@Data
	public static final class QuantileBean implements Serializable {
		@NonNull
		double probability;
		@NonNull
		double quantile;
		@NonNull
		int game_type;
	}

	private final int[] gameTypes = { 204, 205, 214 };

	private final double[] probablities = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9 };

	@Override
	public void run(SparkSession ses) {

		List<QuantileBean> allQuantile = new ArrayList<RatingQuantileJob.QuantileBean>();
		for (int gameType : gameTypes) {
			double[] quantiles = computeQuantile(ses, gameType);
			for (int i = 0; i < probablities.length; i++) {
				allQuantile.add(new QuantileBean(probablities[i], quantiles[i], gameType));
			}
		}
		Dataset<Row> quantilesDs = ses.createDataFrame(allQuantile, QuantileBean.class).withColumn("updated_at",
				functions.current_timestamp());
		//quantilesDs.show(100);
        //save the quantile data into cassandra
		quantilesDs.write()
				.mode("append")
				.format("org.apache.spark.sql.cassandra")
				.option("keyspace", "user_event")
				.option("table", "game_quantile")
				.save();
		log.info("data updated in cassandra");
	}

	private double[] computeQuantile(SparkSession ses, int gameType) {

		Dataset<Row> currentRatigs = DatasetProviderFactory.getInstance()
				.getProvider(DatasetProviderFactory.EXISTING_RATING).provide(ses, gameType);

		double[] quantiles = currentRatigs.stat().approxQuantile("new_rating", probablities, 0);

		return quantiles;

	}

}
