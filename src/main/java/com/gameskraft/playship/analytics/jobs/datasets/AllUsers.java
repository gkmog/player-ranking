package com.gameskraft.playship.analytics.jobs.datasets;

import com.gameskraft.playship.analytics.config.Config;
import com.gameskraft.playship.analytics.config.ConfigFactory;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Function1;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AllUsers implements IDatasetProvider {

    public static final String USER_ID = "id";

    private static final String jdbcQuery = "(select u.user_id from userservice.users u)as user_list";

    @Override
    public Dataset<Row> provide(SparkSession ses, int gameType) {
        return loadFromJDBC(ses, gameType);
    }

    private Dataset<Row> loadFromJDBC(SparkSession ses, int gameType) {

        Config config = ConfigFactory.getConfig();
        Map<String, String> options = new HashMap<String, String>();
        options.put("driver", "com.mysql.jdbc.Driver");
        options.put("user", config.getJdbcUsernamemog());
        options.put("password", config.getJdbcPwdmog());
        options.put("url", config.getJdbcURLmog());
        options.put("dbtable", jdbcQuery);
        SQLContext sqlContext = ses.sqlContext();
        Dataset<Row> df = sqlContext.read().format("jdbc").options(options).load();
        df.printSchema();

        Dataset<Row> dfConverted = df.map((Function1<Row, Row> & Serializable) ((row) -> {
            long id = ((Number) row.getAs("user_id")).longValue();

            return RowFactory.create(id);
        }), RowEncoder.apply(getSchema()));

        return dfConverted;
    }

    private StructType getSchema() {

        StructType schema = new StructType(
                new StructField[]{new StructField(USER_ID, DataTypes.LongType, false, Metadata.empty())}
        );
        return schema;
    }
}
