package com.gameskraft.playship.analytics.jobs;

import org.apache.spark.sql.SparkSession;

public interface ISparkJob {

	public void run(SparkSession ses);
}
