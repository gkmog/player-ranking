package com.gameskraft.playship.analytics.jobs;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public class JobFactory {

	@Getter
	private static final JobFactory instance = new JobFactory();

	private static final Map<String, ISparkJob> jobs = new HashMap<String, ISparkJob>();

	static {
		jobs.put("playerrankingjob", new PlayerRankingJob());
		jobs.put("ratingquantilejob", new RatingQuantileJob());
	}

	public static ISparkJob getJob(String name) {
		return jobs.get(name);
	}

}
