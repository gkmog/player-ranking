package com.gameskraft.playship.analytics.jobs;

public interface Glick2Constants {
	
	public static final double g2factor = 173.7178;
	public static final double g2baseRank = 1500;

}
