package com.gameskraft.playship.analytics;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;

import com.gameskraft.playship.analytics.jobs.JobFactory;

public class Driver {

	private static final class CLIOptions {
		String jobname;
		String master = "local[4]";

		public CLIOptions(String... args) {
			this.jobname = args[0];
			if (args.length > 1)
				this.master = args[1];
		}

		public void validate() {
			if (StringUtils.isEmpty(jobname)) {
				throw new IllegalArgumentException("job name must be provide");
			}
		}
	}

	public static void main(String... args) {
		CLIOptions options = new CLIOptions(args);
		options.validate();
		//
		SparkConf sparkConf = ConfigProvider.getSparkConf();
		sparkConf.setAppName(options.jobname);
		//
		SparkSession ses = new SparkSession(new SparkContext(sparkConf));
		// get the job and run
		JobFactory.getInstance().getJob(options.jobname).run(ses);
		ses.close();
	}

}
