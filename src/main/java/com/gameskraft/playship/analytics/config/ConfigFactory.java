package com.gameskraft.playship.analytics.config;

public class ConfigFactory {

	private static final String STAGE = "stage";
	private static final String PROD = "prod";

	public static Config getConfig() {
		String env = getEnv();
		env = env == null ? STAGE : PROD;

		if (env.equals(STAGE)) {
			return Config.builder().assandraConn("10.0.56.93").cassandraPwd("").cassandraUser("") //cassandra-1.prod | 13.235.24.47
					.jdbcURL("jdbc:mysql://jarvis-slave.c7q7n4ksymis.ap-south-1.rds.amazonaws.com:3306/jarvis?useSSL=false")
					.jdbcUsername("admin")
					.jdbcPwd("m0g#2018")
					.jdbcURLmog("jdbc:mysql://gamezydbslave.c7q7n4ksymis.ap-south-1.rds.amazonaws.com:3306/userservice?useSSL=false")
					.jdbcUsernamemog("gamezyroot")
					.jdbcPwdmog("gamezyroot")
					.mongoInputUri("mongodb://schedular:schedular@mongo.prod:27017/mog") //mongo.prod | 52.66.115.3
					.mongoInputDatabase("mog")
					.mongoInputCollection("user_rating")
					.mongoOutputUri("mongodb://schedular:schedular@mongo.prod:27017/mog")
					.mongoOutputDatabase("mog")
					.mongoOutputCollection("user_rating")
					.build();
		}

		return null;
	}

	public static String getEnv() {
		return System.getenv("ENV");

	}

}
