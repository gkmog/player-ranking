package com.gameskraft.playship.analytics.config;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class Config {

	@NonNull
	private String assandraConn;
	@NonNull
	private String cassandraUser;
	@NonNull
	private String cassandraPwd;

	private String jdbcURL;

	private String jdbcUsername;

	private String jdbcPwd;

	private String jdbcURLmog;

	private String jdbcUsernamemog;

	private String jdbcPwdmog;

	private String mongoInputUri;

	private String mongoInputDatabase;

	private String mongoInputCollection;

	private String mongoOutputUri;

	private String mongoOutputDatabase;

	private String mongoOutputCollection;

}
