package com.gameskraft.playship.analytics;

import org.apache.spark.SparkConf;

import com.gameskraft.playship.analytics.config.Config;
import com.gameskraft.playship.analytics.config.ConfigFactory;

public class ConfigProvider {
	
	public static SparkConf getSparkConf() {
		Config systemConfig = ConfigFactory.getConfig();
		SparkConf  conf = new SparkConf();

		//conf.setMaster("local[*]");
		conf.set("spark.cassandra.connection.host", systemConfig.getAssandraConn());
		conf.set("spark.mongodb.input.uri", systemConfig.getMongoInputUri());
		conf.set("spark.mongodb.input.database", systemConfig.getMongoInputDatabase());
		conf.set("spark.mongodb.input.collection", systemConfig.getMongoInputCollection());
		conf.set("spark.mongodb.input.readPreference.name", "primaryPreferred");
		conf.set("spark.mongodb.output.uri", systemConfig.getMongoOutputUri());
		conf.set("spark.mongodb.output.database", systemConfig.getMongoOutputDatabase());
		conf.set("spark.mongodb.output.collection", systemConfig.getMongoOutputCollection());
		return conf;
	}

}
